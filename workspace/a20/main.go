package main

import "fmt"

type Bar int8

type Foo interface {
	Foo()
}

func (b Bar) first() {
	fmt.Println("first")
}

func (b Bar) second() {
	fmt.Println("second")
}

func (b Bar) third() {
	fmt.Println("third")
}

func (b Bar) Foo() {
}

func main() {

	var b, c Bar

	ab.first()
	ab.second()
	ab.third()

	var fn func(Bar)

	var me func(Foo)

	fn = Bar.first
	// me = Bar.Foo

	fn(b)
	fn(c)
}
