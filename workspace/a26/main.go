package main

type server struct {
	state int
	work  chan func()
}

func newServer() *server {
	return &server{
		state: 42,
		work:  make(chan func()),
	}
}

func (s *server) run() {

	for fn := range s.work {
		fn()
	}
}

func (s *server) currentState() (state int) {
	done := make(chan bool)
	s.work <- func() {
		state = s.state
		close(done)
	}
	<-done
	return
}

func main() {

	s := newServer()
	go s.run()

}
