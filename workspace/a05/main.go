package main

import "fmt"

func short() int { return 32 }

func main() {

	for { // for (;;)
	}

	for cond { // while (cond)
	}

	for ; cond; incr {
	}

	for x := 1; x <= 10; x++ {
	}

	// do { ... } while (cond)
	for {

		if !cond {
			break // continue
		}
	}

label:
	for {
		for {
			for {
				break label // continue
			}
		}
	}

	for {
		goto label
		// goto inner

		for {
		inner:
		}
	}

	if cond {
	}

	if x := f(); x > 10 {
		_ = x
	}

	var x int
	switch x {
	case 1:
		fmt.Println(1)
		fallthrough
	case 2:
		fmt.Println(1)
	}

	switch x {
	case 1, 2:
		fmt.Println(1)
	default:
	}

	switch x := f(); x {
	}

	switch {
	case x < 10:
	case y < 100:
	default:
	}

	if x := f(); x < 10 {
		// x
	} else if y < 100 {
		// x
	} else {
		// x
	}
	// !x

	/*
		if cond:
		  return a
		else:
		  return b
	*/

	if cond {
		return a
	}
	return b

}
