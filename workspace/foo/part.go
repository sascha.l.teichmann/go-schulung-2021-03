package foo

import "fmt"

func init() {
	fmt.Println("init from foo")
}

func init() {
	fmt.Println("init from foo")
}

func init() {
	fmt.Println("init from foo")
}

func Foo() {
	fmt.Println("hello from foo")
}
