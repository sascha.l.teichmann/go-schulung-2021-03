package main

import (
	"fmt"
	"log"
	"regexp"
)

var myRe = regexp.MustCompile(`(\d+)`)

func baz() {
	panic("PANIC!")
}

func bar() {
	baz()
}

func foo() {
	bar()
}

func MyAPI() (x int, err error) {

	defer func() {
		fmt.Println("Hello from defer 1")
		if x := recover(); x != nil {
			err = fmt.Errorf("Panic caught: %v\n", x)
		}

	}()

	foo()

	return 42, nil
}

func main() {
	if v, err := MyAPI(); err != nil {
		log.Fatalf("error: %v\n", err)
	} else {
		fmt.Println(v)
	}
}
