package main

import (
	"fmt"
	"os"
	"strconv"
)

func do(arr [9]int) {
}

func main() {

	arr := [10]int{1: 32, 2: 34, 4: 90}

	arr[0] = 42
	idx, _ := strconv.Atoi(os.Args[1])
	arr[idx] = 43

	for range arr {
		fmt.Println("xx")
	}

	do(arr)
}
