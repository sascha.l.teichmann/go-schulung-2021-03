package main

import "fmt"

type Base int
type SecondBase float32

func (b Base) baseFunc() {
	fmt.Println("baseFunc")
}

type Derived struct {
	Base
	SecondBase
}

func (d Derived) derivedFunc() {
	d.baseFunc()
	fmt.Println("derivedFunc")
}

func (d Derived) baseFunc() {
	d.Base.baseFunc()
}

func main() {

	var b Base
	b.baseFunc()

	var d Derived

	d.derivedFunc()

}
