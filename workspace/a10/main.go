package main

import "fmt"

func main() {

	m := map[string]int{
		"klaus": 32,
		"otto":  0,
	}

	m["petra"] = 42

	fmt.Printf("%+v\n", m)

	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}

	fmt.Printf("otto points: %d\n", m["otto"])
	fmt.Printf("jürgen points: %d\n", m["jürgen"])

	for _, key := range []string{"otto", "jürgen"} {
		if v, ok := m[key]; ok {
			fmt.Printf("%s: %d\n", key, v)
		} else {
			fmt.Printf("%s was not there\n", key)
		}
	}

}
