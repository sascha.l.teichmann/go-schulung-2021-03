package main

import "fmt"

type mySt struct {
	a float64
	b bool
	_ [10]byte

	y struct {
		c int
	}
}

func main() {

	//big := [100_000_000]struct{}{}

	x := mySt{
		b: true,
	}

	x.a = 34.2
	x.y.c = 42

	fmt.Printf("%+v\n", x)
}
