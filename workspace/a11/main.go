package main

import (
	"fmt"
	"math"
)

func variadic(x int, y float64, s ...string) {

	for _, v := range s {
		fmt.Println(v)
	}
}

func main() {
	fmt.Println("vim-go", 45, 223890, math.Pi)

	sl := []string{"Hanna", "Berta"}

	variadic(1, 1, sl...)
}
