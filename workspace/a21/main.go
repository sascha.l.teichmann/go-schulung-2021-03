package main

type C float64

func (c *C) Pointer() {
	if c != nil {
		*c = 32
	}
}

func main() {
	//var c C

	var cp *C // nil

	cp.Pointer()
}
