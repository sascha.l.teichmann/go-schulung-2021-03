package main

import "fmt"

func do(fn func()) {
	fn()
}

func doit() {
	fmt.Println("doit")
}

func create(name string) func() {
	var counter int
	return func() {
		fmt.Printf("created %s %d\n", name, counter)
		counter++
	}
}

func generate() func() (int, bool) {

	count := 3
	return func() (int, bool) {
		count--
		return 42, count > 0
	}
}

func main() {
	var x func() = doit
	do(x)
	c := create("World")
	do(c)
	do(c)
	do(c)

	m := generate()

	for x, ok := m(); ok; x, ok = m() {
		fmt.Println(x)
	}
}
