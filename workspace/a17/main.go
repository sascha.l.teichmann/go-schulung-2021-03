package main

import "fmt"

type BreakType interface {
	Stop()
}

type Car interface {
	BreakType
	Start()
}

func drive(c Car) {
	c.Start()
	c.Stop()
}

type Motor rune

func (m Motor) Start() {
	fmt.Println("Motor start")
}

type Break string
type BrokenBreak string

func (b Break) Stop() {
	fmt.Println("Break Stop")
}

func (bb BrokenBreak) Stop() {
	fmt.Println("Crash!")
}

func main() {

	//var c Car
	var m Motor
	var b BrokenBreak

	drive(struct {
		Motor
		BreakType
	}{m, b})
}
