package main

import (
	"fmt"
	"runtime"
	"sync"
)

func worker(wg *sync.WaitGroup, jobs <-chan int, num int) {
	defer wg.Done()

	for i := range jobs {
		fmt.Printf("%d: %d\n", num, i)
	}
}

func main() {

	jobs := make(chan int)

	var wg sync.WaitGroup

	n := runtime.NumCPU()
	for i := 0; i < n; i++ {
		i := i
		wg.Add(1)
		// go worker(&wg, jobs, i)
		go func() {
			defer wg.Done()

			for j := range jobs {
				fmt.Printf("%d: %d\n", i, j)
			}
		}()
	}

	for i := 1; i <= 10000; i++ {
		jobs <- i
	}
	close(jobs)

	wg.Wait()
}
