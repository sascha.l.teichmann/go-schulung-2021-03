package main

import "fmt"

type Bar interface {
	Bar()
}

type MyFoo interface {
	//Bar
	Foo()
}

func do(mf MyFoo) {
	mf.Foo()
}

type X int

func (x X) Foo() {
	fmt.Println("X: Foo")
}

func main() {

	var x X

	do(x)
}
