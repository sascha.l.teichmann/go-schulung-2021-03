package main

import (
	"fmt"
)

func main() {

	done := make(chan struct{})

	go func() {
		// defer func() { done <- struct{}{} }()
		defer close(done)
		fmt.Println("Hello from Go-Routine")

		//...
	}()

	_, ok := <-done
	fmt.Println("After Go-Routine", ok)

	// time.Sleep(time.Second)
}
