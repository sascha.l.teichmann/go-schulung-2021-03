package main

import "fmt"

func main() {
	done := make(chan struct{})
	ch := make(chan int, 2)

	go func() {
		defer close(done)

		for x := range ch {
			fmt.Printf("in: %d\n", x)
		}
	}()

	ch <- 42
	ch <- 63
	close(ch)

	<-done
}
