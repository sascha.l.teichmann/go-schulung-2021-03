package main

import "fmt"

func main() {

	x := "He世界llo, World"

	fmt.Printf("%c\n", x[2])

	fmt.Println(x)

	for i, c := range x {
		fmt.Printf("%d: %c\n", i, c)
	}

	fmt.Printf("%+q\n", []rune(x))
}
