# Live-Coding Bespiel

* [a01](a01) Primitive Datentypen
* [a02](a02) Operatoren
* [a03](a03) Punktionen, Zeiger
* [a04](a04) Funktionen First-Class-Citizen, Closures
* [a05](a05) Kontrollstrukturen
* [a06](a06) Arrays
* [a07](a07) Slices
* [a08](a08) Strukturen
* [a09](a09) Strings
* [a10](a10) Maps
* [a11](a11) Variadische Funktionen
* [a12](a12) defer/panic/recover
* [a13](a13) Typen, Methoden
* [a14](a14) Konstanten, iota
* [a15](a15) Interfaces
* [a16](a16) Embedding
* [a17](a17) Structual Typing, Komposite
* [a18](a18) Type assertions, Type Switches
* [a19](a19) Method values
* [a20](a20) Method expressions
* [a21](a21) Pointer Receiver und Nil-Pointer-Defererenz
* [a22](a22) Channels, <-
* [a23](a23) Channels, range, close
* [a24](a24) Fan-out
* [a25](a25) Channel of Channels
* [a26](a26) Function channels
* [foo](foo) Ein einfaches Paket.
