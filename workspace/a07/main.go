package main

import "fmt"

func main() {
	var slice []int // nil

	for _, v := range slice {
		fmt.Println(v)
	}

	//var arr [20]bool
	//sl := arr[:]

	slice2 := make([]int, 3, 10)
	for i := range slice2 {
		fmt.Println(i)
	}
	fmt.Printf("len: %d\n", len(slice2))
	fmt.Printf("cap: %d\n", cap(slice2))
	slice3 := slice2[:len(slice2)+1]
	fmt.Printf("len: %d\n", len(slice3))
	fmt.Printf("cap: %d\n", cap(slice3))
	slice2[0] = 3
	fmt.Println(slice3[0])

	var ptrs []*fatObject

	slice3 = append(slice3, 2, 3, 4)
}
