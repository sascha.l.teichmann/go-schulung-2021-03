package main

const pi float32 = 3.1415

func main() {

	var x int
	var y float64

	y = float64(pi)

	x = 100_0_00

	_ = x
	_ = y

	z := int32(6) // -> int

	// *, +, /, %, ^, ~, <<, >>

	z++ // z = z + 1 // z += 1

	// && || !

	z = z + 3

	const e = 2.16
}
