package main

import "fmt"

type Foo complex64

func (f Foo) method() {
	fmt.Println("method")
}

func doit(fn func()) {
	fn()
}

func main() {
	var f Foo
	f.method()
	doit(f.method)

	doit(func() { f.method() })
}
