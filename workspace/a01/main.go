package main

import "fmt"

func main() {

	var (
		bar      int = 42
		baz      float32
		boo, bal int64
	)
	fmt.Println(bar)

	_ = baz
	_, _ = boo, bal

	boo, bal = bal, boo

	// int  // 32bit 64bit vorzeichen
	// uint // "    "     ohnevorz.

	// int8, int16, int32, int64
	// uint8, uint16, uint32, uint64

	// uint8 = byte
	// rune = int32

	// float32, float64

	// complex64, complex128

	// bool

	// string

	var v int8
	var w int16

	w = int16(v)

	_ = w

	//var int float64
}
