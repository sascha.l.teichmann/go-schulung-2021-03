package main

import (
	"fmt"
	"strconv"
)

type A int

func (a A) String() string {
	return strconv.Itoa(int(a))
}

func myPrint(x ...interface{}) {

	for _, v := range x {

		// Type assertiation

		if y, ok := v.(int); ok {
			fmt.Printf("%T\n", y)
		}

		// Type switch

		switch y := v.(type) {
		case int:
			fmt.Println("int", y)
		case string:
			fmt.Println("string", y)
		case fmt.Stringer:
			fmt.Println("stringer: " + y.String())
		}
	}
}

func main() {

	var a A

	myPrint(1)
	myPrint(1, "String", 3.1415, a)
}
