package main

type server struct {
	state int
	work  chan chan int
}

func newServer() *server {
	return &server{
		state: 42,
		work:  make(chan chan int),
	}
}

func (s *server) run() {

	for ch := range s.work {
		ch <- state
	}
}

func (s *server) currentState() int {
	ch := make(chan int)
	s.work <- ch
	return <-ch
}

func main() {

	s := newServer()
	go s.run()

}
