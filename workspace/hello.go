package main

import (
	"fmt"

	_ "github.com/lib/pq"
	_ "github.com/s-l-teichmann/work/foo"
)

func main() {
	fmt.Println("Hello, world!")
}
