package main

import "fmt"

type myType bool

func (mt myType) foo() {
	fmt.Println(mt)
	//mt = false
}

func (myType) bar() {
	fmt.Println("bar")
}

func (mt *myType) modify() {
	*mt = false
}

func main() {

	var x myType

	x = true

	x.foo()
	x.bar()
	x.modify()
	x.foo()

}
