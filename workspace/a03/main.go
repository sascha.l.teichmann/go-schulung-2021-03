package main

import (
	"fmt"
	"math"
)

func foo(x int, y float64, z float64) (float64, bool) {
	z = 12
	return float64(x) * y * z, true
	//w = float64(x) * y * z
	//return
}

func bar(x *float64) {
	*x = 42
}

func newMyFancyStuff() *int {
	var x int
	return &x
}

func main() {

	e := math.E

	foo(32, math.Pi, e)

	bar(&e)

	fmt.Println(e)

}
